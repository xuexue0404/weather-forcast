require('./bootstrap')
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap'
import { createApp, nextTick } from 'vue'
import weatherList from './components/weatherList'
import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import VueAxios from 'vue-axios'

const app = createApp({})
app.use(VueAxios, axios)
app.use(PrimeVue);
app.use(ToastService);
app.component('weather-list', weatherList)

app.mount('#app')

