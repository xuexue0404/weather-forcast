<?php

namespace App\Http\Controllers;
use App\Models\Weather;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $key = config('weather.api_key');
        $weathers = Weather::all();

        return view('welcome', compact('key','weathers'));
    }

    public function list(){
        $weathers = Weather::all();
        return response()->json([
            'message' => 'Successfully retrieve weather list.',
            'data' => $weathers,
        ]);
    }

    public function create(Request $request){
        $weather = new Weather();
        $weather->city = $request->city;
        $weather->interval = $request->interval;
        $weather->save();
        return response()->json([
            'message' => 'Successfully created new city weather.',
            'data' => $weather,
        ]);
    }

    public function destroy(Request $request){
        $remove=Weather::where('city',$request->city)->delete();
        return response()->json([
            'message' => 'Successfully remove city.',
            'data' => $remove,
        ]);
    }
}
