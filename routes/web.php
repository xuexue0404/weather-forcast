<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name('home.index');
Route::get('/list', 'App\Http\Controllers\HomeController@list')->name('home.list');

Route::post('/addnew', 'App\Http\Controllers\HomeController@create')->name('home.addnew');
Route::post('/remove', 'App\Http\Controllers\HomeController@destroy')->name('home.remove');


