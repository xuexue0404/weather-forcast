# Welcome to Weather Forcast

# Installation
open terminal run this to clone repo
`git clone https://gitlab.com/xuexue0404/weather-forcast.git`

cd into weather-forcast
`cd weather-forcast`

run below line in terminal:
`npm install`
`composer install  `

copy and rename file `.env.example` to `.env`

Update .env file to put in DB credential and API key for openweathermap
`DB_CONNECTION=mysql  `
`DB_HOST=127.0.0.1  `
`DB_PORT=3306  `
`DB_DATABASE=  `
`DB_USERNAME=  `
`DB_PASSWORD=`
`API_KEY_WEATHER=// your openweathermap API key`

Run the below command to generate key and migrate table
`php artisan key:generate  `
`php artisan migrate  `

lastly, start your server with
`php artisan serve  `
open browser  http://127.0.0.1:8000/

# User Guide
Key in city name and select the interval for refresh the data and click 'Add'
